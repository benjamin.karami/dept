import { homeSagas } from 'ducks'

const rootSagas = [...homeSagas]

export default rootSagas
