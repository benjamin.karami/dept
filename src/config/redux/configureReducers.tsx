import { combineReducers } from 'redux';

const configureReducers = (rootReducers: { loading: (state: {}, action: { payload?: any; type?: any }) => ({ [p: string]: null }) }) =>
  combineReducers({
    ...rootReducers,
  });
export default configureReducers;
