import { loadingReducers, homeReducers, messageReducers } from 'ducks'

const rootReducers = {
    loading: loadingReducers,
    message: messageReducers,
    home: homeReducers,
}

export default rootReducers
