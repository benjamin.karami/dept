import React from 'react'
interface Props {
    fill: string
}
const CrossIcon = ({ fill }: Props) => {
    return (
        <svg
            width="40"
            height="40"
            viewBox="0 0 25 25"
            fill="none"
            xmlns="http://www.w3.org/2000/svg"
        >
            <path
                d="M2.37375e-05 15.2063L15.2064 0L16.7353 1.52895L1.52898 16.7353L2.37375e-05 15.2063Z"
                fill={fill}
            />
            <path
                d="M1.52895 3.43578e-05L16.7353 15.2064L15.2063 16.7353L0 1.52899L1.52895 3.43578e-05Z"
                fill={fill}
            />
        </svg>
    )
}

CrossIcon.defaultProps = {
    fill: 'white',
}

export default CrossIcon
