import React from 'react'
interface Props {
    fill: string
}
const Ellipse = ({ fill }: Props) => {
    return (
        <svg
            width="12"
            height="12"
            viewBox="0 0 12 12"
            fill="none"
            xmlns="http://www.w3.org/2000/svg"
        >
            <circle cx="6" cy="6" r="5.7" stroke={fill} strokeWidth="0.6" />
            <circle
                cx="6"
                cy="6"
                r="1.7"
                fill={fill}
                stroke={fill}
                strokeWidth="0.6"
            />
        </svg>
    )
}

Ellipse.defaultProps = {
    fill: 'white',
}

export default Ellipse
