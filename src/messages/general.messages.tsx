export default {
    SEE_MORE: 'Read more',
    NAME_FIELD_IS_REQUIRED: 'Name field is required',
    EMAIL_FIELD_IS_REQUIRED: 'Email field is required',
    MESSAGE_FIELD_IS_REQUIRED: 'Message field is required',
    NAME: 'NAME',
    EMAIL: 'EMAIL',
    MESSAGE: 'MESSAGE',
    SEND: 'SEND',
    QUESTION: 'QUESTION?\n' + '\n' + 'WE ARE HERE\n' + '\n' + 'TO HELP!',
    FORM_SUBMITTED: 'FORM SUBMITTED',
}
