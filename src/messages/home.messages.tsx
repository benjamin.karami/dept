import React from 'react'
export default {
    TOP_HOME_SECTION_TITLE: () => (
        <p>
            A selection of projects that
            <strong> pioneer </strong>
            <br></br>
            <strong> tech </strong>and
            <strong> marketing </strong>
            to help brands <br></br> stay ahead.
        </p>
    ),
    WORK_TITLE: 'work',
    CLIENTS: {
        CLIENTS_TITLE: 'Clients',
        CLIENTS_DESCRIPTION:
            'We value a great working relationship with our clients above all else. It’s why they often come to our parties. It’s also why we’re able to challenge and inspire them to reach for the stars.',
    },
}
