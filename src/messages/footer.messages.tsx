export default {
    FOOTER_INFO_LINKS: [
        { title: 'Chamber of Commerce: 63464101', link: '' },
        { title: 'VAT: NL 8552.47.502.B01', link: '' },
        { title: 'Terms and conditions', link: 'terms' },
        { title: '© 2022 Dept Agency', link: '' },
    ],
    TOP: 'TOP',
}
