export default {
    HEADER_TITLES: [
        'work',
        'culture',
        'services',
        'insights',
        'careers',
        'contact',
    ],
    MENU: 'MENU',
}
