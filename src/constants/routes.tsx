const routes = {
    home: '/',
    article: '/article/:criterion',
    client: '/client/:criterion/:article?',
}

export default routes
