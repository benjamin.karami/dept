const apiBaseUri = 'https://62dfc66a976ae7460bf36a65.mockapi.io/api/v1'

const api = {
    base: `${apiBaseUri}`,
}

const endpoints = {
    HOME: {
        MENUS: () => `${api.base}/menus`,
    },
}

export default endpoints
