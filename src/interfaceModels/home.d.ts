export interface Item {
    image: string
    reference_type: string
    reference_target: string
    title: string
    description: string
    id: number
}

export interface Menu {
    id: number
    items: Item[]
    type: string
}

export interface Article {
    title: string
    note: string
    referenceTarget: string
    id: number
}

interface Logo {
    id: number
    image: string
    title: string
}
