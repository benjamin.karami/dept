import React, { useEffect, useLayoutEffect, useMemo } from 'react'
import { connect } from 'react-redux'
import HomeTopSection from 'components/Hero'
import ContactForm from 'components/ContactForm'
import DynamicContent from 'components/DynamicContent'
import { homeActions, homeSelectors } from 'ducks'
import GLOBALS from 'constants/globals'
interface Props {
    menus: any[]
    getMenusApiStatus: string
    getHomeMenus: any
}

const Home = ({ menus, getMenusApiStatus, getHomeMenus }: Props) => {
    useLayoutEffect(() => {
        if (
            !menus.length &&
            getMenusApiStatus !== GLOBALS.API_STATUSES.REQUEST
        ) {
            getHomeMenus()
        }
    }, [menus])

    return useMemo(
        () => (
            <>
                <HomeTopSection />
                <DynamicContent menus={menus} />
                <ContactForm />
            </>
        ),
        [menus]
    )
}

const mapStateToProps = (state: any) => ({
    menus: homeSelectors.getMenus(state),
    getMenusApiStatus: homeSelectors.getHomeMainApiStatus(state),
})

export default connect(mapStateToProps, {
    getHomeMenus: homeActions.getHomeMainRequest,
})(Home)
