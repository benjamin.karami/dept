import React from 'react'
import { Ellipse } from 'resources/svg'
import { GeneralMessages } from 'messages'
import { StyledLink } from './styles'
interface Props {
    link: string
}

const ReadMoreLink = ({ link }: Props) => (
    <StyledLink to={link}>
        <Ellipse />
        {GeneralMessages.SEE_MORE}
    </StyledLink>
)
export default ReadMoreLink
