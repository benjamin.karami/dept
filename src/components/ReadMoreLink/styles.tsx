import styled from 'styled-components'
import { Link } from 'react-router-dom'
import theme from 'globals/theme'

export const StyledLink = styled(Link)`
    font-size: 16px;
    text-decoration: none;
    color: ${theme.colors.white.light};
    text-shadow: 0 0 4px black;

    svg {
        margin-right: 16px;
    }
`
