import styled from 'styled-components'
import theme from 'globals/theme'

export const StyledContactWrapper = styled.div`
    padding: 40px 10px;
    @media (min-width: 992px) {
        padding: 80px 58px;
    }
`

export const StyledContainer = styled.div`
    display: flex;
    align-items: flex-start;
    justify-content: flex-start;
    flex-direction: column;
    @media (min-width: 992px) {
        flex-direction: row;
    }
`

export const StyledTitle = styled.p`
    color: ${theme.colors.black.main};
    width: 100%;
    text-align: center;
    font-size: 30px;
    margin: 0 0 20px 0;
    @media (min-width: 992px) {
        font-size: 30px;
        margin-bottom: 0;
        width: 20%;
        text-align: left;
    }
`

export const StyledForm = styled.form`
    display: flex;
    flex-wrap: wrap;
    flex: 1;
    padding-right: 20px;
    padding-left: 20px;
    flex-direction: column;
    width: 100%;
    @media (min-width: 992px) {
        padding-right: 0;
        padding-left: 100px;
        flex-direction: row;
    }
`

export const StyledInputWrapper = styled.div`
    margin-bottom: 28px;
    width: 100%;
    @media (min-width: 992px) {
        margin-bottom: 80px;
        width: 50%;
        padding-right: 50px;
    }
    > .message {
        textarea {
            height: 100px;
        }
    }
`

export const StyledButtonWrapper = styled.div`
    width: 100%;
`
