import React, { useState } from 'react'
import PropTypes from 'prop-types'
import Input from 'components/Input'
import Button from 'components/Button'
import { GeneralMessages } from 'messages'
import {
    StyledContactWrapper,
    StyledContainer,
    StyledTitle,
    StyledForm,
    StyledInputWrapper,
    StyledButtonWrapper,
} from './styles'

const Contact = () => {
    const [name, setName] = useState('')
    const [email, setEmail] = useState('')
    const [message, setMessage] = useState('')
    const [errors, setErrors] = useState({
        message: '',
        email: '',
        name: '',
    })

    const handleValidation = () => {
        const errorData = {
            name: '',
            email: '',
            message: '',
        }
        if (!name) errorData.name = GeneralMessages.NAME_FIELD_IS_REQUIRED
        if (!email) errorData.email = GeneralMessages.EMAIL_FIELD_IS_REQUIRED
        if (!message)
            errorData.message = GeneralMessages.MESSAGE_FIELD_IS_REQUIRED
        return errorData
    }

    const handleSubmit = (e: any) => {
        e.preventDefault()
        const validationMessages = handleValidation()
        setErrors(validationMessages)
        if (name.length && email.length && message.length) {
            console.log(GeneralMessages.FORM_SUBMITTED)
        }
    }
    return (
        <StyledContactWrapper>
            <StyledContainer>
                <StyledTitle>{GeneralMessages.QUESTION}</StyledTitle>
                <StyledForm onSubmit={handleSubmit}>
                    <StyledInputWrapper>
                        <Input
                            label={GeneralMessages.NAME}
                            type="text"
                            value={name}
                            onChange={(e: {
                                target: { value: React.SetStateAction<string> }
                            }) => setName(e.target.value)}
                            error={errors.name || ''}
                        />
                    </StyledInputWrapper>
                    <StyledInputWrapper>
                        <Input
                            label={GeneralMessages.EMAIL}
                            type="email"
                            value={email}
                            onChange={(e: {
                                target: { value: React.SetStateAction<string> }
                            }) => setEmail(e.target.value)}
                            error={errors.email || ''}
                        />
                    </StyledInputWrapper>
                    <StyledInputWrapper>
                        <Input
                            label={GeneralMessages.MESSAGE}
                            value={message}
                            onChange={(e: {
                                target: { value: React.SetStateAction<string> }
                            }) => setMessage(e.target.value)}
                            error={errors.message || ''}
                            as="textarea"
                            className="message"
                        />
                    </StyledInputWrapper>
                    <StyledButtonWrapper>
                        <Button type="submit">{GeneralMessages.SEND}</Button>
                    </StyledButtonWrapper>
                </StyledForm>
            </StyledContainer>
        </StyledContactWrapper>
    )
}

Contact.propTypes = {
    className: PropTypes.string,
}
Contact.defaultProps = {
    className: '',
}

export default Contact
