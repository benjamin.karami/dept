import React, { Fragment } from 'react'
import { Route, Routes } from 'react-router-dom'
import routers from './routers'

const Router = () => {
    const pages = routers.map(
        ({ layout: Layout = Fragment, component: Component, ...route }) => (
            <Route
                key={`${route.path.replace(/\//g, '_')}_${
                    route.exact && 'exact'
                }`}
                element={
                    <Layout>
                        <Component />
                    </Layout>
                }
                {...route}
            />
        )
    )
    return <Routes>{pages}</Routes>
}

export default Router
