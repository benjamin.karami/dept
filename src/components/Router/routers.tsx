import { HomePage } from 'pages'
import routes from 'constants/routes'
import Layout from 'components/Layout'

const routers = [
    {
        path: routes.home,
        exact: true,
        component: HomePage,
        layout: Layout,
    },
]

export default routers
