import styled from 'styled-components'
import theme from 'globals/theme'

export const StyledNoteCardWrapper = styled.div`
    color: ${theme.colors.white.light};
    font-size: 16px;
    &:not(:first-child) {
        border-top: 1px solid ${theme.colors.white.main};
        margin-top: 35px;
        padding-top: 30px;
    }
    width: 100%;
`
export const StyledTitle = styled.p``

export const StyledNote = styled.p`
    margin: 12px 0;
    overflow: hidden;
    text-overflow: ellipsis;
    display: -webkit-box;
    -webkit-line-clamp: 2;
    line-clamp: 2;
    -webkit-box-orient: vertical;
`
