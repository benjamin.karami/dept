import React from 'react'
import ReadMoreLink from 'components/ReadMoreLink'
import { generatePath } from 'react-router-dom'
import routes from 'constants/routes'
import { Article } from 'interfaceModels/home'
import { StyledNoteCardWrapper, StyledTitle, StyledNote } from './styles'

const NoteCard = ({ article }: { article: Article }) => {
    const { title, note, referenceTarget } = article

    const link = generatePath(routes.article, {
        criterion: referenceTarget,
    })

    return (
        <StyledNoteCardWrapper>
            <StyledTitle>{title}</StyledTitle>
            <StyledNote>{note}</StyledNote>
            <ReadMoreLink link={link} />
        </StyledNoteCardWrapper>
    )
}

export default NoteCard
