import React from 'react'
import NoteCard from 'components/ClientNoteSection/partials/ClinetNoteCard/partials/NoteCard'
import { Article } from 'interfaceModels/home'
import { StyledClientNoteCardWrapper } from './styles'
interface Props {
    articles: Article[]
}

const ClientNoteCard = ({ articles }: Props) => {
    return (
        <StyledClientNoteCardWrapper>
            {articles.map((article) => (
                <NoteCard key={article.id} article={article} />
            ))}
        </StyledClientNoteCardWrapper>
    )
}

export default ClientNoteCard
