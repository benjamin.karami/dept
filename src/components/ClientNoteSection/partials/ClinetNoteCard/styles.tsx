import styled from 'styled-components'
import theme from 'globals/theme'

export const StyledClientNoteCardWrapper = styled.div`
    background: ${theme.colors.black.background};
    display: flex;
    flex-direction: column;
    align-items: flex-start;
    justify-content: center;
    height: 570px;
    padding: 90px 25px;
    width: 100%;

    @media (min-width: 992px) {
        width: 40%;
        padding: 90px 35px;
    }
`
