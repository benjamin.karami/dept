import React from 'react'
import ClientCard from 'components/ClientCardSection/partials/ClientCard'
import ClientsNoteCard from 'components/ClientNoteSection/partials/ClinetNoteCard'
import { StyledClientNoteSectionWrapper } from './styles'
import { Item, Article } from 'interfaceModels/home'

interface Props {
    menu: {
        item: Item
        articles: Article[]
        type: string
        id: number
        isArticleLeft: boolean
    }
}

const ClientCardSection = ({ menu }: Props) => {
    const { item, articles, isArticleLeft } = menu

    return (
        <StyledClientNoteSectionWrapper articleLeft={isArticleLeft}>
            <ClientCard fromNote item={item} />
            <ClientsNoteCard articles={articles} />
        </StyledClientNoteSectionWrapper>
    )
}

export default ClientCardSection
