import styled from 'styled-components'
interface StyleProps {
    articleLeft?: boolean
}

export const StyledClientNoteSectionWrapper = styled.div<StyleProps>`
    display: flex;
    flex-direction: ${(props) =>
        props.articleLeft ? 'column-reverse' : 'column'};
    width: 100%;

    @media (min-width: 992px) {
        flex-direction: ${(props) =>
            props.articleLeft ? 'row-reverse' : 'row'};
    }
`
