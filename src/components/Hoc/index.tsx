import React from 'react'
import classNames from 'classnames'
import { ThemeProvider } from 'styled-components'
import theme from 'globals/theme'
import Router from 'components/Router'
import GlobalStyles from 'globals/styles'

import { StyledHocWrapper } from './styles'

interface Props {
    className?: string
}

const Hoc = ({ className }: Props) => {
    return (
        <ThemeProvider theme={theme}>
            <GlobalStyles />
            <StyledHocWrapper className={classNames(className)}>
                <Router />
            </StyledHocWrapper>
        </ThemeProvider>
    )
}

Hoc.defaultProps = {
    className: '',
}

export default Hoc
