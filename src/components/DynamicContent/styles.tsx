import styled from 'styled-components'

export const StyledDynamicContentSkeletonWrapper = styled.div`
    padding-right: 1.2rem;
    padding-left: 1.2rem;
`

export const StyledDynamicContentWrapper = styled.div`
    > * {
        padding-top: 1.8rem;
    }
`
