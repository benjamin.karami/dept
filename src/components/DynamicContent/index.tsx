import React, { useMemo, memo, lazy, Suspense } from 'react'

import ClientCardSectionSkeleton from 'components/ClientCardSection/ClientCardSectionSkeleton'

const partials = Object.freeze({
    CLIENT: {
        component: lazy(() => import('components/ClientCardSection')),
        fallback: <ClientCardSectionSkeleton />,
    },
    QUOTE: {
        component: lazy(() => import('components/ClientQuoteSection')),
        fallback: null,
    },
    NOTE: {
        component: lazy(() => import('components/ClientNoteSection')),
        fallback: null,
    },
    CLIENT_LIST: {
        component: lazy(() => import('components/ClientList')),
        fallback: null,
    },
})
interface Props {
    menus: any[]
}

const DynamicContent = memo(({ menus }: Props) => {
    const filteredMenus = menus.filter(
        (item) => !!partials[item.type.toUpperCase() as keyof typeof partials]
    )
    const content = useMemo(
        () =>
            filteredMenus.map((menu) => {
                const partial =
                    partials[menu.type.toUpperCase() as keyof typeof partials]
                const { component: Component, fallback } = partial

                return (
                    <Suspense key={menu.id} fallback={fallback}>
                        <Component menu={menu} />
                    </Suspense>
                )
            }),
        [filteredMenus]
    )

    const render = useMemo(() => content, [content])

    return <>{render}</>
})

export default DynamicContent
