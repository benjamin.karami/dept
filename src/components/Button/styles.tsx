import styled from 'styled-components'
import theme from 'globals/theme'
interface StyledProp {
    status?: string
    color?: string
}

export const StyledButton = styled.button<StyledProp>`
    display: flex;
    justify-content: center;
    align-items: center;
    padding: 16px 64px;
    background-color: ${theme.colors.white.light};
    color: ${theme.colors.black.background};
    border: 1px solid ${theme.colors.black.background};
    border-radius: 50px;
    font-size: 18px;
    cursor: pointer;
    transition: all 0.3s ease;

    &:hover {
        background: ${theme.colors.black.background};
        color: ${theme.colors.white.light};
    }
`
