import React from 'react'
import { StyledButton } from './styles'

interface Props {
    color?: string
    status?: string
    type?: string
    children: string
}
const Button = ({ color, status, children }: Props) => (
    <StyledButton color={color} status={status}>
        {children}
    </StyledButton>
)

Button.defaultProps = {
    status: 'black',
    color: 'white',
}

export default Button
