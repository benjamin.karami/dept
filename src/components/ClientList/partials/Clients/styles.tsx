import styled from 'styled-components'

export const StyledClientLogosWrapper = styled.div`
    display: grid;
    grid-template-columns: 1fr 1fr;
    justify-items: center;
    grid-gap: 10px;
    img {
        &:nth-child(n + 3) {
            margin-top: 80px;
        }
        max-width: 100%;
    }

    @media (min-width: 992px) {
        padding: 0 200px;
        display: grid;
        grid-template-columns: 1fr 1fr 1fr;
        grid-gap: 10px;
        img {
            &:nth-child(n + 4) {
                margin-top: 80px;
            }
            &:nth-child(3) {
                margin-top: unset;
            }
        }
    }
`
