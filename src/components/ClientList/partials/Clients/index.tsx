import React from 'react'
import { Logo } from 'interfaceModels/home'
import { StyledClientLogosWrapper } from './styles'
interface Props {
    clientLogos: Logo[]
}

const Clients = ({ clientLogos }: Props) => {
    return (
        <StyledClientLogosWrapper>
            {clientLogos.map((clientLogo) => (
                <img key={clientLogo.id} src={clientLogo.image} />
            ))}
        </StyledClientLogosWrapper>
    )
}

export default Clients
