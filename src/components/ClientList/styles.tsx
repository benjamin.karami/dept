import styled from 'styled-components'
import theme from 'globals/theme'

export const StyledClientListWrapper = styled.div`
    background: ${theme.colors.black.background};
    padding: 90px 45px;

    @media (max-width: 380px) {
        padding: 90px 20px;
    }
`

export const StyledHeaderWrapper = styled.div`
    margin-bottom: 65px;
    color: ${theme.colors.white.light};
`

export const StyledDescription = styled.p`
    font-size: 18px;
    line-height: 18px;
`

export const StyledTitle = styled.p`
    margin-bottom: 32px;
    font-size: 34px;
`
