import React from 'react'
import Clients from 'components/ClientList/partials/Clients'
import { Logo } from 'interfaceModels/home'
import {
    StyledDescription,
    StyledClientListWrapper,
    StyledHeaderWrapper,
    StyledTitle,
} from './styles'
import Skeleton from 'react-loading-skeleton'
interface Props {
    menu: {
        logos: Logo[]
        type: string
        title: string
        description: string
    }
}
const ClientList = ({ menu }: Props) => {
    const { logos, title, description } = menu
    return (
        <StyledClientListWrapper>
            <StyledHeaderWrapper>
                <StyledTitle>{title || <Skeleton />}</StyledTitle>
                <StyledDescription>
                    {description || <Skeleton />}
                </StyledDescription>
            </StyledHeaderWrapper>
            <Clients clientLogos={logos} />
        </StyledClientListWrapper>
    )
}

export default ClientList
