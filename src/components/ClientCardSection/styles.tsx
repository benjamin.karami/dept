import styled from 'styled-components'

export const StyledClientCardWrapper = styled.div`
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;

    @media (min-width: 992px) {
        flex-direction: row;
        justify-content: space-between;
    }
`
