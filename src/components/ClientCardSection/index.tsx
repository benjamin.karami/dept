import React from 'react'
import ClientCard from 'components/ClientCardSection/partials/ClientCard'
import { StyledClientCardWrapper } from './styles'
import { Menu, Item } from 'interfaceModels/home'

interface Props {
    menu: Menu
}

const ClientCardSection = ({ menu }: Props) => {
    const { items } = menu

    return (
        <StyledClientCardWrapper>
            {items.map((item: Item) => (
                <ClientCard key={item.id} item={item} />
            ))}
        </StyledClientCardWrapper>
    )
}

export default ClientCardSection
