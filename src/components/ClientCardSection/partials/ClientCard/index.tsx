import React from 'react'
import { Item } from 'interfaceModels/home'
import { generatePath } from 'react-router-dom'
import routes from 'constants/routes'
import { StyledCardWrapper, StyledTitle, StyledDescription } from './styles'
import ReadMoreLink from 'components/ReadMoreLink'
import Skeleton from 'react-loading-skeleton'

interface Props {
    item: Item
    fromNote?: boolean
}

const ClientCard = ({ item, fromNote }: Props) => {
    const {
        image,
        reference_target: referenceTarget,
        description,
        title,
    } = item

    const link: string = generatePath(routes.article, {
        criterion: referenceTarget,
    })

    return (
        <StyledCardWrapper imageUrl={image} fromNote={fromNote}>
            <StyledTitle>{title || <Skeleton />}</StyledTitle>
            <StyledDescription>{description || <Skeleton />}</StyledDescription>
            <ReadMoreLink link={link} />
        </StyledCardWrapper>
    )
}

export default ClientCard
