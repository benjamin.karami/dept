import styled from 'styled-components'
import theme from 'globals/theme'
interface StyleProps {
    imageUrl: string
    fromNote?: boolean
}

export const StyledCardWrapper = styled.div<StyleProps>`
    width: 100%;
    height: 300px;
    background-size: 100% 100%;
    background-image: url(${(props) => props.imageUrl});
    padding: 20px;
    display: flex;
    flex-direction: column;
    align-items: flex-start;
    justify-content: flex-end;
    color: ${theme.colors.white.light};

    @media (min-width: 992px) {
        width: ${(props) => (props.fromNote ? '60%' : '50%')};
        height: 570px;
        padding: 35px;
    }
`

export const StyledTitle = styled.p`
    text-shadow: 0 0 4px black;
    font-size: 16px;
`

export const StyledDescription = styled.p`
    text-shadow: 0 0 4px black;
    font-size: 25px;
    margin: 16px 0;

    @media (min-width: 992px) {
        font-size: 48px;
    }
`
