import Skeleton, { SkeletonTheme } from 'react-loading-skeleton'
const ClientCardSectionSkeleton = () => {
    return (
        <SkeletonTheme baseColor="#202020" highlightColor="#444">
            <Skeleton
                width="50%"
                height="100%"
                baseColor="#202020"
                highlightColor="#444"
            />
        </SkeletonTheme>
    )
}

export default ClientCardSectionSkeleton
