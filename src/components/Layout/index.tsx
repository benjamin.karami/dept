import React from 'react'
import classNames from 'classnames'
import { StyledLayoutWrapper } from './styles'
import { Content, Header, Footer } from './partials'

interface Props {
    className?: string
    children: React.ReactNode
}

const Layout = ({ className, children }: Props) => {
    return (
        <StyledLayoutWrapper className={classNames(className)}>
            <Header />
            <Content>{children}</Content>
            <Footer />
        </StyledLayoutWrapper>
    )
}

Layout.defaultProps = {
    className: '',
}

export default Layout
