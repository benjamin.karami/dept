import React, { useEffect, useLayoutEffect } from 'react'
import { HeaderMessages } from 'messages'
import {
    StyledFullPageMenuWrapper,
    StyledLinksWrapper,
    StyledFullNavLink,
    HeaderWrapper,
    StyledCrossIcon,
} from './styles'
import { StyledLogo } from 'components/Layout/partials/Header/styles'
interface Props {
    setIsFullMenuOpen: Function
    isFullMenuOpen: boolean
}

const FullPageMenu = ({ isFullMenuOpen, setIsFullMenuOpen }: Props) => {
    useLayoutEffect(() => {
        if (isFullMenuOpen) {
            document.body.style.overflow = 'hidden'
        } else {
            document.body.style.overflow = 'unset'
        }
    }, [isFullMenuOpen])

    const generatedLink = (path: string) => {
        return path.toLowerCase()
    }

    return (
        <StyledFullPageMenuWrapper isFullMenuOpen={isFullMenuOpen}>
            <HeaderWrapper onClick={() => setIsFullMenuOpen(false)}>
                <StyledLogo />
                <StyledCrossIcon />
            </HeaderWrapper>
            <StyledLinksWrapper>
                {HeaderMessages.HEADER_TITLES.map((title) => (
                    <StyledFullNavLink
                        key={`nav-${title}`}
                        to={generatedLink(title)}
                    >
                        {title.toUpperCase()}
                    </StyledFullNavLink>
                ))}
            </StyledLinksWrapper>
        </StyledFullPageMenuWrapper>
    )
}

export default FullPageMenu
