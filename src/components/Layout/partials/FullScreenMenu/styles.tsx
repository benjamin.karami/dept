import styled from 'styled-components'
import { Link } from 'react-router-dom'
import { CrossIcon } from 'resources/svg'
import theme from 'globals/theme'
interface StyledProp {
    isFullMenuOpen: boolean
}
export const StyledFullPageMenuWrapper = styled.div<StyledProp>`
    position: absolute;
    left: 0;
    top: -100%;
    bottom: 0;
    right: 0;
    width: 100%;
    height: 100vh;
    background: rgba(0, 0, 0, 0.85);
    opacity: 0;
    padding: 45px 55px;
    transition: all 0.8s ease-in-out;
    z-index: 1;

    svg {
        cursor: pointer;
    }

    top: ${(props) => props.isFullMenuOpen && '0'};
    opacity: ${(props) => props.isFullMenuOpen && '1'};
`

export const StyledLinksWrapper = styled.div`
    display: flex;
    flex-direction: column;
    align-items: flex-start;
    justify-content: flex-start;
`

export const StyledCrossIcon = styled(CrossIcon)``

export const StyledFullNavLink = styled(Link)`
    color: ${theme.colors.white.light};
    font-size: 20px;
    padding: 10px 0;
    width: 100%;
    text-align: right;
    position: relative;
    transition: 0.3s;

    &:not(:last-child) {
        &:before {
            position: absolute;
            bottom: 0;
            right: 0;
            content: '';
            height: 1px;
            background: ${theme.colors.white.light};
            width: 100%;
            transition: 0.3s;
        }

        &:hover {
            &:before {
                width: 50%;
            }
        }
    }

    @media (min-width: 992px) {
        font-size: 40px;
    }
`

export const HeaderWrapper = styled.div`
    display: flex;
    align-items: center;
    justify-content: space-between;
    margin-bottom: 60px;
    background: ${theme.colors.black.dark};
`
