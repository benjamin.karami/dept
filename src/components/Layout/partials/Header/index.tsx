import React, { useState } from 'react'
import { HeaderMessages } from 'messages'
import {
    StyledHeaderWrapper,
    StyledLogo,
    StyledLinksWrapper,
    StyledHeaderLink,
    StyledMobileFullMenu,
} from './styles'
import FullPageMenu from 'components/Layout/partials/FullScreenMenu'
import BurgerMenu from 'components/Layout/partials/Header/BurgerMenu'
import headerMessages from '../../../../messages/header.messages'

const Header = () => {
    const [isFullMenuOpen, setIsFullMenuOpen] = useState(false)
    const generatedLink = (path: string) => {
        return path.toLowerCase()
    }

    return (
        <>
            <FullPageMenu
                isFullMenuOpen={isFullMenuOpen}
                setIsFullMenuOpen={setIsFullMenuOpen}
            />
            <StyledHeaderWrapper>
                <StyledLogo />
                <StyledMobileFullMenu onClick={() => setIsFullMenuOpen(true)}>
                    {headerMessages.MENU}
                </StyledMobileFullMenu>
                <StyledLinksWrapper>
                    {HeaderMessages.HEADER_TITLES.map((title) => (
                        <StyledHeaderLink
                            key={`header${title}`}
                            to={generatedLink(title)}
                        >
                            {title.toUpperCase()}
                        </StyledHeaderLink>
                    ))}
                    <BurgerMenu onClick={() => setIsFullMenuOpen(true)} />
                </StyledLinksWrapper>
            </StyledHeaderWrapper>
        </>
    )
}

export default Header
