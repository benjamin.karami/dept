import styled from 'styled-components'
import { Link } from 'react-router-dom'
import theme from 'globals/theme'
import { DeptLogo } from 'resources/svg'

export const StyledHeaderWrapper = styled.div`
    display: flex;
    align-items: center;
    justify-content: space-between;
    padding: 45px 55px;
    background: ${theme.colors.black.background};
    position: fixed;
    width: 100%;
    top: 0;
`

export const StyledLogo = styled(DeptLogo)``

export const StyledMobileFullMenu = styled.p`
    display: none;
    font-size: 16px;
    color: #ffffff;
    @media (max-width: 810px) {
        display: unset;
    }
`

export const StyledDot = styled.div`
    width: 8px;
    height: 8px;
    border-radius: 50%;
    background-color: ${theme.colors.white.light};
    transition: 0.3s;
    &.middle {
        margin: 0 3px;
    }
`

export const StyledBurgerMenuWrapper = styled.div`
    display: flex;
    cursor: pointer;
    justify-content: space-around;
    width: 45px;
    &:hover {
        span {
            width: 9px;
            height: 9px;
        }
    }
`

export const StyledLinksWrapper = styled.div`
    height: 40px;
    display: none;
    align-items: center;

    @media (min-width: 810px) {
        display: flex;
    }
`

export const StyledHeaderLink = styled(Link)`
    font-weight: 400;
    font-size: 16px;
    color: ${theme.colors.white.light};
    margin-right: 20px;
    position: relative;

    &:before {
        content: '';
        height: 1px;
        bottom: 0;
        left: 0;
        background-color: ${theme.colors.white.light};
        width: 0;
        position: absolute;
        transition: 0.3s ease-out;
    }

    &:hover {
        &:before {
            width: 100%;
        }
    }
`
