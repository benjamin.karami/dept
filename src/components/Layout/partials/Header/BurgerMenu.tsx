import React from 'react'
import { StyledBurgerMenuWrapper, StyledDot } from './styles'

const BurgerMenu = ({ onClick }: { onClick: () => void }) => {
    return (
        <StyledBurgerMenuWrapper onClick={onClick}>
            <StyledDot />
            <StyledDot className="middle" />
            <StyledDot />
        </StyledBurgerMenuWrapper>
    )
}

export default BurgerMenu
