import React from 'react'
import { HeaderMessages, FooterMessages } from 'messages'
import { StyledHeaderLink } from 'components/Layout/partials/Header/styles'
import { ArrowTopIcon, Instagram, Facebook, Twitter } from 'resources/svg'
import {
    StyledFooterWrapper,
    StyledLogo,
    StyledMenuLinksWrapper,
    StyledInfoWrapper,
    StyledInfoLinks,
    StyledLinksWrapper,
    StyledGoToTop,
    StyledContainer,
    StyledSocialMediaLinksWrapper,
} from './styles'

const Footer = () => {
    const generatedLink = (path: string) => {
        return path.toLowerCase()
    }

    return (
        <StyledContainer>
            <StyledFooterWrapper>
                <StyledMenuLinksWrapper>
                    <div className="dept-logo">
                        <StyledLogo />
                    </div>
                    <StyledLinksWrapper>
                        {HeaderMessages.HEADER_TITLES.map((title: string) => (
                            <StyledHeaderLink
                                key={`footer${title}`}
                                to={generatedLink(title)}
                            >
                                {title}
                            </StyledHeaderLink>
                        ))}
                    </StyledLinksWrapper>
                    <StyledSocialMediaLinksWrapper>
                        <Instagram />
                        <Twitter />
                        <Facebook />
                    </StyledSocialMediaLinksWrapper>
                </StyledMenuLinksWrapper>
                <StyledInfoWrapper>
                    {FooterMessages.FOOTER_INFO_LINKS.map(
                        (item: { link: string; title: string }) => (
                            <StyledInfoLinks
                                key={item.title}
                                to={generatedLink(item.link)}
                            >
                                {item.title}
                            </StyledInfoLinks>
                        )
                    )}
                </StyledInfoWrapper>
            </StyledFooterWrapper>
            <StyledGoToTop onClick={() => window.scrollTo(0, 0)}>
                <ArrowTopIcon />
                <span>{FooterMessages.TOP}</span>
            </StyledGoToTop>
        </StyledContainer>
    )
}

export default Footer
