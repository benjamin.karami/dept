import styled from 'styled-components'
import { Link } from 'react-router-dom'
import { DeptLogo } from 'resources/svg'
import theme from '../../../../globals/theme'

export const StyledFooterWrapper = styled.div`
    padding: 40px;
    background: #0e0e0e;
    width: 100%;

    @media (min-width: 992px) {
        padding: 65px 50px 90px;
    }
`

export const StyledLogo = styled(DeptLogo)``

export const StyledMenuLinksWrapper = styled.div`
    margin-bottom: 65px;
    display: flex;
    flex-direction: column;
    justify-content: flex-start;
    align-items: flex-start;
    position: relative;

    .dept-logo {
        display: none;
    }

    @media (min-width: 992px) {
        flex-direction: row;
        align-items: center;

        .dept-logo {
            display: unset;
        }
    }
`

export const StyledLinksWrapper = styled.div`
    display: flex;
    flex-direction: column;
    a {
        margin-top: 10px;
    }

    @media (min-width: 992px) {
        margin-left: 115px;
        flex-direction: row;
        a {
            margin-top: unset;
        }
    }
`

export const StyledInfoWrapper = styled.div`
    border-top: 1px solid #3e3e3e;
    padding-top: 45px;
    display: flex;
    flex-direction: column;
    align-items: flex-start;
    justify-content: flex-start;

    a {
        margin-top: 10px;
    }

    @media (min-width: 992px) {
        display: flex;
        align-items: center;
        flex-direction: row;
        justify-content: flex-start;

        a {
            margin-top: unset;
        }
    }
`

export const StyledInfoLinks = styled(Link)`
    font-weight: 400;
    font-size: 16px;
    color: #808080;
    margin-right: 35px;

    @media (min-width: 992px) {
        &:last-child {
            justify-self: flex-end;
            margin-left: auto;
        }
    }
`

export const StyledSocialMediaLinksWrapper = styled.div`
    position: absolute;
    right: 0;
    top: 0;
    display: flex;
    flex-direction: column;

    svg {
        &:not(:first-child) {
            margin-top: 20px;
        }
    }

    @media (min-width: 992px) {
        display: none;
    }
`

export const StyledGoToTop = styled.div`
    background: ${theme.colors.white.light};
    color: ${theme.colors.blue.main};
    cursor: pointer;
    width: 12rem;
    font-size: 2.2rem;
    font-weight: normal;
    line-height: 1.45;
    flex-direction: column;
    justify-content: center;
    align-items: center;

    span {
        margin-top: 20px;
    }

    display: none;
    @media (min-width: 992px) {
        display: flex;
    }
`

export const StyledContainer = styled.div`
    display: flex;
`
