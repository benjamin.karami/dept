import React from 'react'
import PropTypes from 'prop-types'
import classNames from 'classnames'

import { StyledContentWrapper } from './styles'

interface Props {
    className: string
    children: React.ReactNode
}

const Content = ({ className, children }: Props) => {
    return (
        <StyledContentWrapper className={classNames(className)}>
            {children}
        </StyledContentWrapper>
    )
}

Content.propTypes = {
    className: PropTypes.string,
    children: PropTypes.node.isRequired,
}
Content.defaultProps = {
    className: '',
}

export default Content
