import React from 'react'
import {
    StyledClientQuoteSectionWrapper,
    StyledQuote,
    StyledDescription,
} from './styles'
interface Props {
    menu: {
        id: number
        quote: string
        writer: string
        type: string
    }
}

const ClientQuoteSection = ({ menu }: Props) => {
    const { quote, writer } = menu
    return (
        <StyledClientQuoteSectionWrapper>
            <StyledQuote>{quote}</StyledQuote>
            <StyledDescription>{writer}</StyledDescription>
        </StyledClientQuoteSectionWrapper>
    )
}

export default ClientQuoteSection
