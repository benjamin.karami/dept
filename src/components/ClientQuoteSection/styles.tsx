import styled from 'styled-components'
import theme from 'globals/theme'

export const StyledClientQuoteSectionWrapper = styled.div`
    display: flex;
    justify-content: center;
    align-items: center;
    flex-direction: column;
    padding: 90px 25px;
    background: ${theme.colors.black.background};
    color: ${theme.colors.white.light};

    @media (min-width: 992px) {
        padding: 90px 35px;
    }
`

export const StyledQuote = styled.p`
    font-size: 26px;
    margin-bottom: 32px;
`

export const StyledDescription = styled.p`
    font-size: 18px;
`
