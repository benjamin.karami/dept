import React from 'react'
import { HomeMessages } from 'messages'
import {
    StyledTopSectionWrapper,
    StyledTitle,
    StyledDescription,
} from './styles'

const HomeTopSection = () => {
    return (
        <StyledTopSectionWrapper>
            <StyledTitle>{HomeMessages.WORK_TITLE}</StyledTitle>
            <StyledDescription>
                {HomeMessages.TOP_HOME_SECTION_TITLE()}
            </StyledDescription>
        </StyledTopSectionWrapper>
    )
}

export default HomeTopSection
