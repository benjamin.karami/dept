import styled from 'styled-components'
import homeTopSection from 'resources/img/homeTopSection.png'
import theme from 'globals/theme'
export const StyledTopSectionWrapper = styled.div`
    width: 100%;
    height: 100vh;
    background-size: 100% 100%;
    background-image: url(${homeTopSection});
    padding: 85px 55px;
    display: flex;
    flex-direction: column;
    align-items: flex-start;
    justify-content: flex-end;
`

export const StyledTitle = styled.p`
    font-weight: 400;
    font-size: 16px;
    color: ${theme.colors.white.light};
    margin: 0;
    text-shadow: 0 0 4px black;
`

export const StyledDescription = styled.div`
    font-weight: 400;
    font-size: 20px;
    color: ${theme.colors.white.light};
    margin-top: 35px;
    text-shadow: 0 0 4px black;

    p {
        margin: 0;
    }

    @media (min-width: 992px) {
        font-size: 35px;
    }
`
