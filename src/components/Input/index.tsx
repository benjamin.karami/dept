import React from 'react'
import classNames from 'classnames'
import {
    StyledInputWrapper,
    StyledLabel,
    StyledInput,
    StyledError,
} from './styles'
interface Props {
    label: string
    error: string
    className: string
    type?: string
    value?: string
    onChange: Function
    as?: any
}
const Input = ({
    className,
    label,
    error,
    as,
    value,
    onChange,
    type,
}: Props) => (
    <StyledInputWrapper className={classNames(className)}>
        {!!label && <StyledLabel>{label}</StyledLabel>}
        <StyledInput as={as} value={value} type={type} onChange={onChange} />
        {!!error && <StyledError>{error}</StyledError>}
    </StyledInputWrapper>
)

Input.defaultProps = {
    className: '',
    label: '',
    error: '',
}

export default Input
