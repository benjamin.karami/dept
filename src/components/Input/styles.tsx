import styled from 'styled-components'
import theme from 'globals/theme'
export const StyledInputWrapper = styled.div``
interface StyleProps {
    type?: string
    value?: string
    onChange: Function
    as?: string
}

export const StyledLabel = styled.label`
    font-family: Arial;
    font-size: 15px;
    font-weight: bold;
    font-stretch: normal;
    font-style: normal;
    line-height: 2;
    letter-spacing: normal;
    color: ${theme.colors.black.light};
    display: block;
    margin-bottom: 10px;
`

export const StyledInput = styled.input<StyleProps>`
    font-family: Arial;
    font-size: 15px;
    padding-bottom: 10px;
    display: block;
    width: 100%;
    border: none;
    border-bottom: solid 1px ${theme.colors.white.dark};
`

export const StyledError = styled.div`
    font-family: Arial;
    font-size: 13px;
    font-weight: normal;
    font-stretch: normal;
    font-style: normal;
    line-height: normal;
    letter-spacing: normal;
    color: ${theme.colors.red.main};
    margin-top: 10px;
`
