import { css, createGlobalStyle } from 'styled-components'
import 'sanitize.css'
import 'resources/styles/maison.css'

const additional = css`
    html {
        text-align: left;
        direction: ltr;
        font-size: 62.5%;
        font-family: ${(props) => props.theme.defaultFont}, Arial;
        scroll-behavior: smooth;
        * {
            font-family: inherit;
            outline: none;
            text-decoration: none;
            &::-webkit-scrollbar {
                display: none;
            }
            -ms-overflow-style: none;
            scrollbar-width: none;
        }
    }

    p {
        margin: 0;
    }

    @keyframes fadeIn {
        0% {
            opacity: 0;
        }

        100% {
            opacity: 1;
        }
    }
`

const GlobalStyles = createGlobalStyle`
  ${additional}
`

export default GlobalStyles
