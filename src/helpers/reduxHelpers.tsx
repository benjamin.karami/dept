export const createAction = (type: any) => (payload: any) => ({
  type,
  payload,
});

export const getActionStatus = (type: string) => {
  const matches = /(.*)_(REQUEST|SUCCESS|FAILURE)$/.exec(type);

  if (matches) {
    const [, name, status] = matches;
    return [name, status];
  }
  return [type, ''];
};
