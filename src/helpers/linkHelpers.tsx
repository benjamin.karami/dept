import routes from 'constants/routes'
interface GenerateLink {
    referenceType: string
    referenceTarget: string
}
export const generatedLink = ({
    referenceType,
    referenceTarget,
}: GenerateLink) => {
    const prefixes = {
        ARTICLE: routes.article,
    }

    if (referenceType) {
        if (prefixes[referenceType?.toUpperCase() as keyof typeof prefixes]) {
            return `${
                prefixes[referenceType?.toUpperCase() as keyof typeof prefixes]
            }/${referenceTarget}`
        }
        return referenceTarget
    }
    return ''
}
