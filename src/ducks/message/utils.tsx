import { getActionStatus } from 'helpers'

const clearAlert = (alerts: any[], alert: { [p: string]: any; key: any }) => {
    // @ts-ignore
    const [name] = getActionStatus(alert().type)
    return alerts.filter(({ key }) => key !== name)
}

/**
 * Filter all toasts but this one
 * @param id
 * @param toasts
 * @returns {*}
 */
const filterToasts = (id: any, toasts: any[]): any =>
    toasts.filter((toast) => toast.id !== id)

export default {
    clearAlert,
    filterToasts,
}
