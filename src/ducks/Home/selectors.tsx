import { createSelector } from 'reselect'

import { createActionStatusSelector } from 'ducks/loading/selectors'
import homeActions from './actions'

const getMainMenus = (state: any) => state.home
const getMenus = createSelector(getMainMenus, (home) => home.menus)

const getHomeMainApiStatus = createActionStatusSelector(
    homeActions.getHomeMainRequest
)

const coreSelectors = {
    getMenus,
    getHomeMainApiStatus,
}

export default coreSelectors
