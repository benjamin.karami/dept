import { put, takeLatest } from 'redux-saga/effects'

import { callService, globalSagaCatcher } from 'helpers'
import { homeActions, homeServices, homeTypes } from 'ducks/Home'

function* getHomeMainRequest(action: any): any {
    try {
        const response = yield callService(homeServices.getMenus)
        yield put(homeActions.getHomeMainSuccess(response.data))
    } catch (e) {
        // @ts-ignore
        yield globalSagaCatcher(homeActions.getHomeMainFailure, e, action)
    }
}

const homeSagas = [
    takeLatest(homeTypes.GET_HOME_MAIN_REQUEST, getHomeMainRequest),
]

export default homeSagas
