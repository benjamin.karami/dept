import homeTypes from './types'

export const initialState = {
    menus: [],
}

export default (
    state = initialState,
    action: { payload?: any; type?: any }
) => {
    switch (action.type) {
        case homeTypes.GET_HOME_MAIN_SUCCESS:
            return {
                ...state,
                menus: action.payload,
            }
        case homeTypes.CLEAR_HOME_MAIN:
            return {
                ...state,
                menus: initialState.menus,
            }
        default:
            return initialState
    }
}
