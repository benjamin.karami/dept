import endpoints from 'constants/endpoints'

const articlesServices = {
    getMenus: () => ({
        url: endpoints.HOME.MENUS(),
        method: 'GET',
    }),
}

export default articlesServices
