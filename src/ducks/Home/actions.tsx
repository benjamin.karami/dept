import { createAction } from 'helpers'

import homeTypes from './types'

export default {
    getHomeMainRequest: createAction(homeTypes.GET_HOME_MAIN_REQUEST),
    getHomeMainSuccess: createAction(homeTypes.GET_HOME_MAIN_SUCCESS),
    getHomeMainFailure: createAction(homeTypes.GET_HOME_MAIN_FAILURE),
    clearHomeMain: createAction(homeTypes.CLEAR_HOME_MAIN),
}
