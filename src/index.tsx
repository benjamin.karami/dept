import React from 'react'
import ReactDOM from 'react-dom/client'
import HOC from './components/Hoc'
import { Provider as ReduxProvider } from 'react-redux'
import { BrowserRouter as Router } from 'react-router-dom'
import configureStore from './config/redux/configureStore'
let store = configureStore()

const root = ReactDOM.createRoot(document.getElementById('root') as HTMLElement)

root.render(
    <React.StrictMode>
        <ReduxProvider store={store}>
            <Router>
                <HOC />
            </Router>
        </ReduxProvider>
    </React.StrictMode>
)
